<?php

// Require
require_once __DIR__ . '/Bridge/Doctrine/autoload.php';
require_once __DIR__ . '/Bridge/Monolog/autoload.php';
require_once __DIR__ . '/Bundle/DebugBundle/autoload.php';
require_once __DIR__ . '/Bundle/SecurityBundle/autoload.php';
require_once __DIR__ . '/Bundle/TwigBundle/autoload.php';
require_once __DIR__ . '/Bundle/WebProfilerBundle/autoload.php';
require_once __DIR__ . '/Component/Asset/autoload.php';
require_once __DIR__ . '/Component/AssetMapper/autoload.php';
require_once __DIR__ . '/Component/BrowserKit/autoload.php';
require_once __DIR__ . '/Component/Console/autoload.php';
require_once __DIR__ . '/Component/CssSelector/autoload.php';
require_once __DIR__ . '/Component/Dotenv/autoload.php';
require_once __DIR__ . '/Component/ExpressionLanguage/autoload.php';
require_once __DIR__ . '/Component/Emoji/autoload.php';
require_once __DIR__ . '/Component/Form/autoload.php';
require_once __DIR__ . '/Component/HtmlSanitizer/autoload.php';
require_once __DIR__ . '/Component/HttpClient/autoload.php';
require_once __DIR__ . '/Component/Intl/autoload.php';
require_once __DIR__ . '/Component/Ldap/autoload.php';
require_once __DIR__ . '/Component/Lock/autoload.php';
require_once __DIR__ . '/Component/Mailer/autoload.php';
require_once __DIR__ . '/Component/Messenger/autoload.php';
require_once __DIR__ . '/Component/Notifier/autoload.php';
require_once __DIR__ . '/Component/Process/autoload.php';
require_once __DIR__ . '/Component/PropertyInfo/autoload.php';
require_once __DIR__ . '/Component/RateLimiter/autoload.php';
require_once __DIR__ . '/Component/Scheduler/autoload.php';
require_once __DIR__ . '/Component/Semaphore/autoload.php';
require_once __DIR__ . '/Component/Serializer/autoload.php';
require_once __DIR__ . '/Component/Stopwatch/autoload.php';
require_once __DIR__ . '/Component/String/autoload.php';
require_once __DIR__ . '/Component/Uid/autoload.php';
require_once __DIR__ . '/Component/Validator/autoload.php';
require_once __DIR__ . '/Component/WebLink/autoload.php';
require_once __DIR__ . '/Component/Webhook/autoload.php';
require_once __DIR__ . '/Component/Workflow/autoload.php';
require_once __DIR__ . '/Component/Yaml/autoload.php';
require_once __DIR__ . '/Contracts/autoload.php';

// Suggest

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
// this is an autogenerated file - do not edit
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                ___CLASSLIST___
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require ___BASEDIR___$classes[$cn];
        }
    },
    ___EXCEPTION___,
    ___PREPEND___
);
// @codeCoverageIgnoreEnd

// Files
