#!/bin/bash
set -e


# This test fails on s390x
if [ `dpkg --print-architecture` = 's390x' ] ; then
  echo "Don't fail testsuite on s390x, this test always failed there"
  exit 0
fi

# we only access local servers, so just unset any proxy setting
unset http_proxy
unset no_proxy

BASH_XTRACEFD=1
set -x

patch -p1 < debian/patches/DEP-8/Tests-against-installed-classes.patch

mkdir --parents vendor
cp debian/autoload_runtime.php vendor
phpab   --output vendor/autoload.php \
	--whitelist '*\\tests\\*' \
	--tolerant \
	--template debian/autoload.php.tests.tpl \
	--blacklist 'symfony\\component\\dependencyinjection\\tests\\fixtures\\container\\projectservicecontainer' \
	--exclude 'src/Symfony/Bridge/Doctrine/Tests/ArgumentResolver/EntityValueResolverTest.php' \
	--exclude 'src/Symfony/Bridge/Doctrine/Tests/Fixtures/DoctrineLoaderEmbed.php' \
	--exclude 'src/Symfony/Bridge/Doctrine/Tests/Fixtures/DoctrineLoaderEntity.php' \
	--exclude 'src/Symfony/Bridge/Doctrine/Tests/Fixtures/EmbeddedIdentifierEntity.php' \
	--exclude 'src/Symfony/Bridge/Doctrine/Tests/PropertyInfo/Fixtures/DoctrineWithEmbedded.php' \
	--exclude 'src/Symfony/Bridge/ProxyManager/Tests/LazyProxy/PhpDumper/Fixtures/proxy-implem.php' \
	--exclude 'src/Symfony/Component/DependencyInjection/Tests/Fixtures/php/*' \
	--exclude 'src/Symfony/Component/Messenger/Tests/Exception/HandlerFailedExceptionTest.php' \
	--exclude 'src/Symfony/Component/PropertyInfo/Tests/Extractor/PhpStanExtractorTest.php' \
	--exclude 'src/Symfony/Component/Routing/Tests/Fixtures/AttributesFixtures/*' \
	src/Symfony

# Drop never ending testsuite with PHPUnit 11
rm -f src/Symfony/Component/HttpKernel/phpunit.xml.dist
rm -f src/Symfony/Component/RateLimiter/phpunit.xml.dist

set +x
components=$(find src/Symfony -mindepth 3 -maxdepth 4 -type f -name phpunit.xml.dist -printf '%h\n')
echo "$components" | parallel --gnu --keep-order 'echo -e "\\nRunning {} tests"; SYMFONY_DEPRECATIONS_HELPER=disabled php -d assert.exception=1 -d zend.assertions=1 /usr/bin/phpunit --colors=always --exclude-group jwt --exclude-group network --exclude-group tty --exclude-group benchmark --exclude-group intl-data --exclude-group functional --exclude-group composer --exclude-group nophpunit11 {} || (echo -e "\\e[41mKO\\e[0m {}" && $(exit 1));' && exit_code=0 || exit_code=$?
set -x

patch -Rp1 < debian/patches/DEP-8/Tests-against-installed-classes.patch

exit $exit_code
